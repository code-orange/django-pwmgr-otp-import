from django import forms

from django_mdat_customer.django_mdat_customer.models import MdatCustomers


class OtpImportForm(forms.Form):
    customer = forms.ModelChoiceField(
        label="Customer", queryset=MdatCustomers.objects.all().select_related()
    )
    folder = forms.CharField(label="Folder", max_length=200, initial="Microsoft 365")
    qr_code_file = forms.ImageField()

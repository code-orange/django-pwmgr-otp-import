from django.urls import path

from . import views

urlpatterns = [
    path("", views.import_otp, name="import_otp"),
]

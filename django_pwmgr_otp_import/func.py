import pyotp
from PIL import Image
from pyzbar.pyzbar import decode
from pyzbar.wrapper import ZBarSymbol


def get_data_from_qr_code(image: Image):
    image_data = decode(image, symbols=[ZBarSymbol.QRCODE])

    return image_data[0].data.decode("utf-8")


def parse_otp_url(otp_url: str):
    otp_cred = pyotp.parse_uri(otp_url)

    return otp_cred

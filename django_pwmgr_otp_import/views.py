from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader

from django_pwmgr_client.django_pwmgr_client.func import pwmgr_set_single_credentials
from django_pwmgr_otp_import.django_pwmgr_otp_import.forms import OtpImportForm
from django_pwmgr_otp_import.django_pwmgr_otp_import.func import *


@login_required
def import_otp(request):
    template = loader.get_template("django_pwmgr_otp_import/import_otp.html")

    template_opts = dict()

    # if this is a POST request we need to process the form data
    if request.method == "POST":
        # create a form instance and populate it with data from the request:
        form = OtpImportForm(request.POST, request.FILES)

        # check whether it's valid:
        if form.is_valid():
            customer = form.cleaned_data["customer"]
            qr_code_file = form.cleaned_data.get("qr_code_file")
            folder = form.cleaned_data["folder"]

            otp_url = get_data_from_qr_code(Image.open(qr_code_file))

            otp_data = parse_otp_url(otp_url)

            cred_name = "OTP - " + otp_data.name

            cred_data = {
                "name": cred_name,
                "username": otp_data.name,
                "description": otp_data.name,
                "uri": "https://example.org/",
                "password_cleartext": otp_data.secret,
            }

            pwmgr_response = pwmgr_set_single_credentials(
                settings.MDAT_ROOT_CUSTOMER_ID, customer.org_tag, folder, cred_data
            )

            if pwmgr_response:
                cred_id = pwmgr_response["body"]["id"]

                return HttpResponse(cred_id)
        else:
            print(form.errors)
            return HttpResponse(form.errors)

    form = OtpImportForm()

    template_opts["form"] = form

    return HttpResponse(template.render(template_opts, request))

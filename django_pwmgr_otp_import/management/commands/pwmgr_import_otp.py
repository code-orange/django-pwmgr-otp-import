from django.core.management.base import BaseCommand

from django_pwmgr_otp_import.django_pwmgr_otp_import.func import *


class Command(BaseCommand):
    def handle(self, *args, **options):
        otp_url = get_data_from_qr_code("totp.png")

        otp_data = parse_otp_url(otp_url)

        cred_name = "OTP - " + otp_data.name
        cred_secret = otp_data.secret

        print("Name: " + cred_name)
        print("Secret: " + cred_secret)

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
